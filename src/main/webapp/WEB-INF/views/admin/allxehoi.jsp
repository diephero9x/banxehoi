<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/admin/taglib.jsp"%>
<title>Danh sách xe hơi</title>
<body>
<section class="wrapper">
                <div class="table-agile-info">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Danh sách xe hơi
                        </div>
                        <div><c:if test="${ xe_hoi_phan_trang1.size() > 0 }">
                        
                            <table class="table" ui-jq="footable" ui-options='{
        "paging": {
          "enabled": true
        },
        "filtering": {
          "enabled": true
        },
        "sorting": {
          "enabled": true
        }}'>
                                <thead>
                                    <tr>
                                        <th data-breakpoints="xs">ID xe</th>
                                        <th>Tên Xe</th>
                                        <th>Hãng Xe</th>
                                        <th data-breakpoints="xs">Hình Ảnh</th>

                                        <th data-breakpoints="xs sm md" data-title="DOB">Đơn giá</th>
                                        <th></th>
                                    </tr>
                                </thead>
                                <tbody>
                              <c:forEach var="item" items="${ xe_hoi_phan_trang1 }" varStatus="loop">
                                    <tr data-expanded="true">
                                        <td>${ item.id_san_pham }</td>
                                        <td>${ item.ten_san_pham }</td>
                                        <td>${ item.ten_hang_xe }</td>
                                        <td>${ item.hinh_anh_sp }</td>
                                        <td>${ item.don_gia_san_pham }</td>
                                        <td><a href="" class="active styling-edit" ui-toggle-class="">
                                <i class="fa fa-pencil-square-o text-success text-active"></i>
                            </a></td>
                                    </tr>
                                    </c:forEach>
                                </tbody>
                            </table>
                            </c:if>
                        </div>
                    </div>
                </div>
                <div class="pagination">
		<c:forEach var="item" begin="1" end="${ xe_hoi_pt1.tongTrang }" varStatus="loop">
			<c:if test="${ (loop.index) == xe_hoi_pt1.trangHienTai }">
				<a href="<c:url value="/quan-tri/xe-hoi/${ loop.index }" />" class="active">${ loop.index }</a>
			</c:if>
			<c:if test="${ (loop.index) != xe_hoi_pt1.trangHienTai }">
				<a href="<c:url value="/quan-tri/xe-hoi/${ loop.index }" />">${ loop.index }</a>
			</c:if>
		</c:forEach>
		</div>
            </section>
</body>
</html>