<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Sản phẩm</title>
<style>
.pagination {
    display: flex;
    justify-content: center;
}

.pagination a {
	color: black;
	float: left;
	padding: 8px 16px;
	text-decoration: none;
	transition: background-color .3s;
	border: 1px solid #ddd;
}

.pagination a.active {
	background-color: #dc3535;
	color: white;
	border: 1px solid #dc3535;
}

.pagination a:hover:not (.active ) {
	background-color: #ddd;
}
</style>
</head>
<body>
	<div class="ads-grid py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>D</span>anh <span>S</span>ách Xe Hơi
			</h3>
			<!-- //tittle heading -->

		
			<div class="row">
				<!-- product left -->
				<div class="agileinfo-ads-display col-lg-9">
					<div class="wrapper">
						<!-- first section -->
						<c:if test="${ xe_hoi_phan_trang.size() > 0 }">
							<div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
								<div class="row">
									<c:forEach var="item" items="${ xe_hoi_phan_trang }" varStatus="loop">
										<div class="col-md-4 product-men">
											<div class="men-pro-item simpleCart_shelfItem">
												<div class="men-thumb-item text-center">
													<img
														src="<c:url value="/assets/user/images/${ item.hinh_anh_sp }" />"
														width="100%" height="250px" alt="">
													<div class="men-cart-pro">
														<div class="inner-men-cart-pro">
															<a href="<c:url value="/chi-tiet-xe-hoi/${ item.id_san_pham }" />" class="link-product-add-cart">Quick
																View</a>
														</div>
													</div>
												</div>
												<div class="item-info-product text-center border-top mt-4">
													<h4 class="pt-1">
														<a href="<c:url value="/chi-tiet-xe-hoi/${ item.id_san_pham }" />">${ item.ten_san_pham }</a>
													</h4>
													<div class="info-product-price my-2">
														<span class="item_price">$ ${ item.don_gia_san_pham }</span>
													</div>
													<div
														class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
														<form>
															<fieldset>
																<input
																	type="hidden" name="add" value="1" /> <input
																	type="hidden" name="business" value=" " /> <input
																	type="hidden" name="item_name"
																	value="Samsung Galaxy J7" /> <input type="hidden"
																	name="amount" value="200.00" /> <input type="hidden"
																	name="discount_amount" value="1.00" /> <input
																	type="hidden" name="currency_code" value="USD" /> <input
																	type="hidden" name="return" value=" " /> <input
																	type="hidden" name="cancel_return" value=" " />
																	 <input type="button" name="add-to-cart" data-id_bds="${ item.id_san_pham }" value="Thêm giỏ hàng"
                                                            class="button btn add-to-cart" />
															</fieldset>
														</form>
													</div>

												</div>
											</div>
										</div>
										<c:if
											test="${ (loop.index + 1) % 3 == 0 || (loop.index + 1)  == xe_hoi_phan_trang.size() }">
								</div>
							</div>

							<c:if test="${ (loop.index + 1) < xe_hoi_phan_trang.size() }">
								<div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
									<div class="row">
							</c:if>
						</c:if>
						</c:forEach>
						</c:if>
						<!-- //first section -->

					</div>
				</div>
				<!-- //product left -->
				<!-- product right -->
				<%@include file="/WEB-INF/views/layouts/user/productright.jsp"%>
				<!-- //product right -->
			</div>
		</div>
		<div class="pagination">
		<c:forEach var="item" begin="1" end="${ xe_hoi_pt.tongTrang }" varStatus="loop">
			<c:if test="${ (loop.index) == xe_hoi_pt.trangHienTai }">
				<a href="<c:url value="/xe-hoi/${ idHangXe }/${ loop.index }" />" class="active">${ loop.index }</a>
			</c:if>
			<c:if test="${ (loop.index) != xe_hoi_pt.trangHienTai }">
				<a href="<c:url value="/xe-hoi/${ idHangXe }/${ loop.index }" />">${ loop.index }</a>
			</c:if>
		</c:forEach>
		</div>
	</div>
	</div>
</body>
</html>
