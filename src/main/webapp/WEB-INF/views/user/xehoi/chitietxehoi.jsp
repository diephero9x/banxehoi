<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>
<!DOCTYPE html>
<head>
<meta charset="UTF-8">
<title>Chi tiết sản phẩm</title>

</head>
<body>
	<!-- Single Page -->
	<div class="banner-bootom-w3-agileits py-5">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>C</span>hi <span>T</span>iết <span>X</span>e <span>H</span>ơi

			</h3>
			<!-- //tittle heading -->
			<div class="row">
				<div class="col-lg-5 col-md-8 single-right-left ">
					<div class="grid images_3_of_2">
						<div class="flexslider">
							<ul class="slides">
								<li
									data-thumb="<c:url value="/assets/user/images/${ chi_tiet_xe_hoi.hinh_anh_sp }" />">
									<div class="thumb-image">
										<img
											src="<c:url value="/assets/user/images/${ chi_tiet_xe_hoi.hinh_anh_sp }" />"
											data-imagezoom="true" class="img-fluid" alt="">
									</div>
								</li>
								<li
									data-thumb="<c:url value="/assets/user/images/${ chi_tiet_xe_hoi.hinh_anh_sp }" />">
									<div class="thumb-image">
										<img
											src="<c:url value="/assets/user/images/${ chi_tiet_xe_hoi.hinh_anh_sp }" />"
											data-imagezoom="true" class="img-fluid" alt="">
									</div>
								</li>
								<li
									data-thumb="<c:url value="/assets/user/images/${ chi_tiet_xe_hoi.hinh_anh_sp }" />">
									<div class="thumb-image">
										<img
											src="<c:url value="/assets/user/images/${ chi_tiet_xe_hoi.hinh_anh_sp }" />"
											data-imagezoom="true" class="img-fluid" alt="">
									</div>
								</li>
							</ul>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>

				<div class="col-lg-7 single-right-left simpleCart_shelfItem">
					<h3 class="mb-3">${ chi_tiet_xe_hoi.ten_san_pham }</h3>
					<p class="mb-3">
						<span class="item_price">$${
							chi_tiet_xe_hoi.don_gia_san_pham }</span>

					</p>
					<div class="single-infoagile">
						<ul>
							<li class="mb-3">Số ghế ngồi: ${ chi_tiet_xe_hoi.so_ghe_ngoi }</li>
							<li class="mb-3">Tên hãng xe: ${ chi_tiet_xe_hoi.ten_hang_xe }</li>
							<li class="mb-3">Loại xe: ${ chi_tiet_xe_hoi.ten_loai }</li>
							<li class="mb-3">Xe mới: <c:if
									test="${ chi_tiet_xe_hoi.moi == true }">
							mới
							</c:if> <c:if test="${ chi_tiet_xe_hoi.moi == false }">
							cũ
							</c:if></li>
						</ul>
					</div>
					<div class="product-single-w3l">
						<p class="my-3">
							<i class="far fa-hand-point-right mr-2"></i> <label>Mô tả
							</label> xe hơi:
						</p>
						<ul>
							<li class="mb-1">${ chi_tiet_xe_hoi.mo_ta_san_pham }</li>

						</ul>

					</div>
					<div class="occasion-cart">
						<div
							class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
							<form>
								<fieldset>
									<input type="hidden" name="add" value="1" /> <input
										type="hidden" name="business" value=" " /> <input
										type="hidden" name="item_name" value="Samsung Galaxy J7" /> <input
										type="hidden" name="amount" value="200.00" /> <input
										type="hidden" name="discount_amount" value="1.00" /> <input
										type="hidden" name="currency_code" value="USD" /> <input
										type="hidden" name="return" value=" " /> <input type="hidden"
										name="cancel_return" value=" " /> <input type="button"
										name="add-to-cart"
										data-id_bds="${ chi_tiet_xe_hoi.id_san_pham }"
										value="Thêm giỏ hàng" class="button btn add-to-cart" />
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- //Single Page -->
</body>
