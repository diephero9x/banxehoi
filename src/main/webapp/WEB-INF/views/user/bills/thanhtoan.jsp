<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>

<head>
<meta charset="UTF-8">
<title>Thanh toán</title>
</head>
<body>

	<div class="container">
		<main>
			<div class="py-5 text-center">

				<h2>Checkout form</h2>
			</div>
			<div class="row g-5">

				<div class="col-md-7 col-lg-8">
					<h4 class="mb-3">Thông tin đặt hàng</h4>

					<form:form action="checkout" method="POST" modelAttribute="bills"
						class="creditly-card-form agileinfo_form">
						<div class="creditly-wrapper wthree, w3_agileits_wrapper">
							<div class="information-wrapper">
								<div class="first-row">
									<div class="controls form-group">
										<form:input type="text" path="display_name"
											class="billing-address-name form-control"
											placeholder="Họ và tên" />
									</div>
									<div class="w3_agileits_card_number_grids">
										<div class="w3_agileits_card_number_grid_left form-group">
											<div class="controls">

												<form:input type="text" path="phone"
													class="billing-address-name form-control"
													placeholder="Số điện thoại" />
											</div>
										</div>
										<div class="w3_agileits_card_number_grid_right form-group">
											<div class="controls">
												<form:input type="text" path="address"
													class="billing-address-name form-control"
													placeholder="Địa chỉ" />
											</div>
										</div>
									</div>
									<div class="controls form-group">
										<form:input type="email" path="user"
											class="billing-address-name form-control" placeholder="Email" />
									</div>
									<div class="controls form-group">
										<form:input type="text" path="note"
											class="billing-address-name form-control" placeholder="Ghi chu" />
									</div>
								</div>
								<button class="submit check_out btn" type="submit"
									name="gui_thong_tin_dat_coc">Đặt cọc</button>
							</div>
						</div>
					</form:form>
				</div>
			</div>
		</main>

		<footer class="my-5 pt-5 text-muted text-center text-small">
			<p class="mb-1">© 2017–2021 Company Name</p>
			<ul class="list-inline">
				<li class="list-inline-item"><a href="#">Privacy</a></li>
				<li class="list-inline-item"><a href="#">Terms</a></li>
				<li class="list-inline-item"><a href="#">Support</a></li>
			</ul>
		</footer>
	</div>
</body>
