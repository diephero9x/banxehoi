<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>
<title>Trang chủ</title>
<body>

	<div class="ads-grid py-sm-5 py-4">
		<div class="container py-xl-4 py-lg-2">
			<!-- tittle heading -->
			<h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
				<span>O</span>ur <span>N</span>ew <span>P</span>roducts
			</h3>
			<h1>${ statusLogin }</h1>
			<!-- //tittle heading -->
			<div class="row">
				<!-- product left -->
				<div class="agileinfo-ads-display col-lg-9">
					<div class="wrapper">
						<!-- first section -->
						<div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
							<h3 class="heading-tittle text-center font-italic">Xe Hơi
								Mới</h3>
							<c:if test="${ san_pham_moi.size() > 0 }">
								<div class="row">
									<c:forEach var="item" items="${ san_pham_moi }"
										varStatus="loop]">
										<div class="col-md-4 product-men mt-5">
											<div class="men-pro-item simpleCart_shelfItem">
												<div class="men-thumb-item text-center">
													<img
														src="<c:url value="/assets/user/images/${ item.hinh_anh_sp }" />"
														width="100%" height="250px" alt="">
													<div class="men-cart-pro">
														<div class="inner-men-cart-pro">
															<a href="chi-tiet-xe-hoi/${ item.id_san_pham }"
																class="link-product-add-cart">Xem chi tiết</a>
														</div>
													</div>
												</div>
												<div class="item-info-product text-center border-top mt-4">
													<h4 class="pt-1">
														<a href="chi-tiet-xe-hoi/${ item.id_san_pham }">${ item.ten_san_pham }</a>
													</h4>
													<div class="info-product-price my-2">
														<span class="item_price">$ <fmt:formatNumber
																type="number" groupingUsed="true"
																value="${ item.don_gia_san_pham }" /></span>

													</div>
													<div
														class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
														<form>
															<fieldset>
																<input type="hidden" name="add" value="1" /> <input
																	type="hidden" name="business" value=" " /> <input
																	type="hidden" name="item_name"
																	value="Samsung Galaxy J7" /> <input type="hidden"
																	name="amount" value="200.00" /> <input type="hidden"
																	name="discount_amount" value="1.00" /> <input
																	type="hidden" name="currency_code" value="USD" /> <input
																	type="hidden" name="return" value=" " /> <input
																	type="hidden" name="cancel_return" value=" " /> <input
																	type="button" name="add-to-cart"
																	data-id_bds="${ item.id_san_pham }"
																	value="Thêm giỏ hàng" class="button btn add-to-cart" />
															</fieldset>
														</form>
													</div>
												</div>
											</div>
										</div>
									</c:forEach>

								</div>
							</c:if>
						</div>
						<!-- //first section -->
						<!-- second section -->
						<div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mb-4">
							<h3 class="heading-tittle text-center font-italic">Xe Hơi
								Nổi Bật</h3>
							<c:if test="${ san_pham_noibat.size() > 0 }">

								<div class="row">
									<c:forEach var="item" items="${ san_pham_noibat }"
										varStatus="loop]">
										<div class="col-md-4 product-men mt-5">
											<div class="men-pro-item simpleCart_shelfItem">
												<div class="men-thumb-item text-center">
													<img
														src="<c:url value="/assets/user/images/${ item.hinh_anh_sp }" />"
														width="100%" height="250px" alt="">
													<div class="men-cart-pro">
														<div class="inner-men-cart-pro">
															<a href="chi-tiet-xe-hoi/${ item.id_san_pham }"
																class="link-product-add-cart">Xem chi tiết</a>
														</div>
													</div>
												</div>
												<div class="item-info-product text-center border-top mt-4">
													<h4 class="pt-1">
														<a href="chi-tiet-xe-hoi/${ item.id_san_pham }">${ item.ten_san_pham }</a>
													</h4>
													<div class="info-product-price my-2">
														<span class="item_price">$ <fmt:formatNumber
																type="number" groupingUsed="true"
																value="${ item.don_gia_san_pham }" /></span>

													</div>
													<div
														class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
														<form>
															<fieldset>
																<input type="hidden" name="add" value="1" /> <input
																	type="hidden" name="business" value=" " /> <input
																	type="hidden" name="item_name"
																	value="Samsung Galaxy J7" /> <input type="hidden"
																	name="amount" value="200.00" /> <input type="hidden"
																	name="discount_amount" value="1.00" /> <input
																	type="hidden" name="currency_code" value="USD" /> <input
																	type="hidden" name="return" value=" " /> <input
																	type="hidden" name="cancel_return" value=" " /> <input
																	type="button" name="add-to-cart"
																	data-id_bds="${ item.id_san_pham }"
																	value="Thêm giỏ hàng" class="button btn add-to-cart" />
															</fieldset>
														</form>
													</div>
												</div>
											</div>
										</div>
									</c:forEach>
								</div>
							</c:if>
						</div>
						<!-- //second section -->
						<!-- third section -->
						<div class="product-sec1 product-sec2 px-sm-5 px-3">
							<div class="row">
								<h3 class="col-md-4 effect-bg">Summer Carnival</h3>
								<p class="w3l-nut-middle">Get Extra 10% Off</p>
								<div class="col-md-8 bg-right-nut">
									<img src="<c:url value="/assets/user/images/image1.png"/>"
										alt="">
								</div>
							</div>
						</div>
						<!-- //third section -->
						<!-- fourth section -->
						<div class="product-sec1 px-sm-4 px-3 py-sm-5  py-3 mt-4">
							<h3 class="heading-tittle text-center font-italic">Large
								Appliances</h3>
							<div class="row">
								<div class="col-md-4 product-men mt-5">
									<div class="men-pro-item simpleCart_shelfItem">
										<div class="men-thumb-item text-center">
											<img src="<c:url value="/assets/user/images/m7.jpg"/>" alt="">
											<div class="men-cart-pro">
												<div class="inner-men-cart-pro">
													<a href="single.html" class="link-product-add-cart">Quick
														View</a>
												</div>
											</div>
										</div>
										<span class="product-new-top">New</span>
										<div class="item-info-product text-center border-top mt-4">
											<h4 class="pt-1">
												<a href="single.html">Whirlpool 245</a>
											</h4>
											<div class="info-product-price my-2">
												<span class="item_price">$230.00</span>
												<del>$280.00</del>
											</div>
											<div
												class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
												<form>
													<fieldset>
														<input type="hidden" name="add" value="1" /> <input
															type="hidden" name="business" value=" " /> <input
															type="hidden" name="item_name" value="Samsung Galaxy J7" />
														<input type="hidden" name="amount" value="200.00" /> <input
															type="hidden" name="discount_amount" value="1.00" /> <input
															type="hidden" name="currency_code" value="USD" /> <input
															type="hidden" name="return" value=" " /> <input
															type="hidden" name="cancel_return" value=" " /> <input
															type="button" name="add-to-cart"
															data-id_bds="${ item.id_san_pham }" value="Thêm giỏ hàng"
															class="button btn add-to-cart" />
													</fieldset>
												</form>
											</div>

										</div>
									</div>
								</div>
								<div class="col-md-4 product-men mt-5">
									<div class="men-pro-item simpleCart_shelfItem">
										<div class="men-thumb-item text-center">
											<img src="<c:url value="/assets/user/images/m8.jpg"/>" alt="">
											<div class="men-cart-pro">
												<div class="inner-men-cart-pro">
													<a href="single.html" class="link-product-add-cart">Quick
														View</a>
												</div>
											</div>
										</div>
										<div class="item-info-product text-center border-top mt-4">
											<h4 class="pt-1">
												<a href="single.html">BPL Washing Machine</a>
											</h4>
											<div class="info-product-price my-2">
												<span class="item_price">$180.00</span>
												<del>$200.00</del>
											</div>
											<div
												class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
												<form>
													<fieldset>
														<input type="hidden" name="add" value="1" /> <input
															type="hidden" name="business" value=" " /> <input
															type="hidden" name="item_name" value="Samsung Galaxy J7" />
														<input type="hidden" name="amount" value="200.00" /> <input
															type="hidden" name="discount_amount" value="1.00" /> <input
															type="hidden" name="currency_code" value="USD" /> <input
															type="hidden" name="return" value=" " /> <input
															type="hidden" name="cancel_return" value=" " /> <input
															type="button" name="add-to-cart"
															data-id_bds="${ item.id_san_pham }" value="Thêm giỏ hàng"
															class="button btn add-to-cart" />
													</fieldset>
												</form>
											</div>

										</div>
									</div>
								</div>
								<div class="col-md-4 product-men mt-5">
									<div class="men-pro-item simpleCart_shelfItem">
										<div class="men-thumb-item text-center">
											<img src="<c:url value="/assets/user/images/m9.jpg"/>" alt="">
											<div class="men-cart-pro">
												<div class="inner-men-cart-pro">
													<a href="single.html" class="link-product-add-cart">Quick
														View</a>
												</div>
											</div>
										</div>
										<div class="item-info-product text-center border-top mt-4">
											<h4 class="pt-1">
												<a href="single.html">Microwave Oven</a>
											</h4>
											<div class="info-product-price my-2">
												<span class="item_price">$199.00</span>
												<del>$299.00</del>
											</div>
											<div
												class="snipcart-details top_brand_home_details item_add single-item hvr-outline-out">
												<form>
													<fieldset>
														<input type="hidden" name="add" value="1" /> <input
															type="hidden" name="business" value=" " /> <input
															type="hidden" name="item_name" value="Samsung Galaxy J7" />
														<input type="hidden" name="amount" value="200.00" /> <input
															type="hidden" name="discount_amount" value="1.00" /> <input
															type="hidden" name="currency_code" value="USD" /> <input
															type="hidden" name="return" value=" " /> <input
															type="hidden" name="cancel_return" value=" " /> <input
															type="button" name="add-to-cart"
															data-id_bds="${ item.id_san_pham }" value="Thêm giỏ hàng"
															class="button btn add-to-cart" />
													</fieldset>
												</form>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<!-- //fourth section -->
					</div>
				</div>
				<!-- //product left -->
				<%@include file="/WEB-INF/views/layouts/user/productright.jsp"%>
				<!-- product right -->

				<!-- //product right -->
			</div>
		</div>
	</div>
	</div>
</body>