<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>
<!DOCTYPE html>
<head>
<meta charset="UTF-8">
<title>Giỏ hàng</title>
</head>
<body>

<div class="privacy py-sm-5 py-4">
        <div class="container py-xl-4 py-lg-2">
            <!-- tittle heading -->
            <h3 class="tittle-w3l text-center mb-lg-5 mb-sm-4 mb-3">
                <span>G</span>iỏ hàng
            </h3>
            <!-- //tittle heading -->
            <div class="checkout-right">
                
                <div class="table-responsive">

                    
                        <table class="timetable_sub">
                            <thead>
                                <tr>
                                    <th>Mã Xe</th>
                                    <th>Hình ảnh</th>
                                    <th>Số lượng</th>
                                    <th>Tên</th>
                                    <th>Tổng Giá</th>
                                    <th>Cập nhật giỏ hàng</th>
                                    <th>Xóa giỏ hàng</th>
                                </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="item" items="${ Cart }"
										varStatus="loop]">
                                        <tr class="rem1">
                                            <td class="invert">${ item.value.xehoi.id_san_pham }</td>
                                            <td class="invert-image">
                                                <a href="<c:url value="/chi-tiet-xe-hoi/${ item.value.xehoi.id_san_pham }" />">
                                                    <img src="<c:url value="/assets/user/images/${ item.value.xehoi.hinh_anh_sp }" />"
                                                        alt="" width="86px" height="86px"
                                                        class="img-responsive">
                                                </a>
                                            </td>
                                            <td class="invert">
                                                <div class="input-group mb-3">
                                                    <input type="number" name=""
                                                        min="1" style="width: 50px; height: 40px" id="quanty-cart-${ item.key }" class="form-control"
                                                        value="${ item.value.soluong }">
                                                    <input type="hidden" name="rowID_cart" class="form-control" value="">
                                                </div>
                                            </td>
                                            <td class="invert">${ item.value.xehoi.ten_san_pham }</td>
                                            <td class="invert">
                                                <p> <fmt:setLocale value = "en_US"/>
                                                <fmt:formatNumber value = "${item.value.tonggiaTien}" type = "currency"/></p>
                                            </td>
                                           <td><button data-id="${ item.key }" class="btn btn-outline-secondary edit-cart"    style="background: #5fcfcf"
                                                value=" type="submit">Cập nhật giỏ hàng</button>
                                            <td class="invert">
                                                <a href="<c:url value="/DeleteCart/${ item.key }" />">
                                                    <img src="<c:url value="/assets/user/images/close_1.png"/>">
                                                </a>
                                            </td>
                                            
                                        </tr>
                               </c:forEach>
                           <tr>
                                        <td>
                                            <p>Tổng tiền: <fmt:setLocale value = "en_US"/>
                                                <fmt:formatNumber value = "${TotalPriceCart}" type = "currency"/> <span></span></p>
                                            <input type="hidden" name="tongtien" value="">
                                        </td>
                                        <td>
                                        <c:if test="${ empty LoginInfo }">
                                            <div class="checkout-right-basket" data-toggle="modal" data-target="#exampleModal">
                                                <a href= "#">Thanh toán
                                                    <span class="far fa-hand-point-right"></span>
                                                </a>
                                            </div>
                                            </c:if>
                                        <c:if test="${ not empty LoginInfo }">
                                            <div class="checkout-right-basket">
                                                <a href='<c:url value="checkout" />'>Thanh toán
                                                    <span class="far fa-hand-point-right"></span>
                                                </a>
                                            </div>
                                            </c:if>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>
                    

                </div>
            </div>

        </div>
    </div>
    <content tag="script">
    <script>
    	$(".edit-cart").on("click", function(){
    		var id = $(this).data("id");
    		var quanty = $("#quanty-cart-"+id).val();
    		window.location = "EditCart/"+id+"/"+quanty;
    	});
    </script>
    </content>
    
</body>
