<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>
<div class="agile-main-top">
	<div class="container-fluid">
		<div class="row main-top-w3l py-2">
			<div class="col-lg-4 header-most-top">
				<a class="text-white text-lg-left text-center" href="index.html">	
				</a>
			</div>
			<div class="col-lg-8 header-right mt-lg-0 mt-2">
				<!-- header lists -->
				<ul>
					<li class="text-center border-right text-white"><a href="#"
						 class="text-white">
							<i class="fas fa-truck mr-2"></i>Chu Văn Điệp
					</a></li>
					<li class="text-center border-right text-white"><i
						class="fas fa-phone mr-2"></i> 033 5659 514</li>
					<c:if test="${ empty LoginInfo }">
						<li class="text-center border-right text-white"><a href="#"
							data-toggle="modal" data-target="#exampleModal"
							class="text-white"> <i class="fas fa-sign-in-alt mr-2"></i>
								Đăng nhập
						</a></li>
						<li class="text-center text-white"><a href="#"
							data-toggle="modal" data-target="#exampleModal2"
							class="text-white"> <i class="fas fa-sign-out-alt mr-2"></i>Đăng
								ký
						</a></li>
					</c:if>
					<c:if test="${ not empty LoginInfo }">
						<li class="text-center border-right text-white">${ LoginInfo.display_name }
						</li>
						<li class="text-center text-white"><a href="<c:url value="/dang-xuat/"></c:url>"
							class="text-white"> <i class="fas fa-sign-out-alt mr-2"></i>Đăng xuất
						</a></li>
					</c:if>
				</ul>
				<!-- //header lists -->
			</div>
		</div>
	</div>
</div>

<!-- Button trigger modal(select-location) -->

<!-- //shop locator (popup) -->
<!-- modals -->
	<!-- log in -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title text-center">Log In</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form:form action="dang-nhap" method="POST" modelAttribute="user">
					<div class="form-group">
						<label class="col-form-label">Email</label>
						<form:input type="email" path="user" class="form-control"
							placeholder=" " />
					</div>
					<div class="form-group">
						<label class="col-form-label">Mật khẩu</label>
						<form:input type="password" path="password" class="form-control"
							placeholder=" " />
					</div>
					<div class="right-w3l">
						<input type="submit" class="form-control" value="Đăng nhập">
					</div>
					<div class="sub-w3l">
						<div class="custom-control custom-checkbox mr-sm-2">
							<input type="checkbox" class="custom-control-input"
								id="customControlAutosizing"> <label
								class="custom-control-label" for="customControlAutosizing">Remember
								me?</label>
						</div>
					</div>
					<p class="text-center dont-do mt-3">
						Don't have an account? <a href="#" data-toggle="modal"
							data-target="#exampleModal2"> Đăng ký ngay</a>
					</p>
				</form:form>
				</div>
			</div>
		</div>
	</div>
	<!-- register -->
	<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title">Register</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>
				<div class="modal-body">
					<form:form action="dang-ky" method="POST" modelAttribute="user">
					<div class="form-group">
						<label class="col-form-label">Họ tên</label>
						<form:input type="text" path="display_name" class="form-control"
							placeholder=" " />
					</div>
					<div class="form-group">
						<label class="col-form-label">Email</label>
						<form:input type="email" path="user" class="form-control"
							placeholder=" " />
					</div>
					<div class="form-group">
						<label class="col-form-label">Mật khẩu</label>
						<form:input type="password" path="password" class="form-control"
							placeholder=" " id="password1" />
					</div>
					<div class="form-group">
						<label class="col-form-label">Nhập lại mật khẩu</label> <input
							type="password" class="form-control" placeholder=" "
							name="Confirm Password" id="password2" required="">
					</div>
					<div class="form-group">
						<label class="col-form-label">Địa chỉ</label>
						<form:input type="text" path="address" class="form-control"
							placeholder=" " />
					</div>
					<div class="right-w3l">
						<input type="submit" class="form-control" value="Register">
					</div>
					<div class="sub-w3l">
						<div class="custom-control custom-checkbox mr-sm-2">
							<input type="checkbox" class="custom-control-input"
								id="customControlAutosizing2"> <label
								class="custom-control-label" for="customControlAutosizing2">I
								Accept to the Terms & Conditions</label>
						</div>
					</div>
				</form:form>
				</div>
			</div>
		</div>
	</div>
	<!-- //modal -->

<!-- //top-header -->

<!-- header-bottom-->
<div class="header-bot">
	<div class="container">
		<div class="row header-bot_inner_wthreeinfo_header_mid">
			<!-- logo -->
			<div class="col-md-3 logo_agile">
				<h1 class="text-center">
					<a href="<c:url value="/"/>" class="font-weight-bold font-italic">
						<img src="<c:url value="/assets/user/images/logo2.png" />" alt=" "
						class="img-fluid">Bán Xe Ô Tô
					</a>
				</h1>
			</div>
			<!-- //logo -->
			<!-- header-bot -->
			<div class="col-md-9 header mt-4 mb-md-0 mb-4">
				<div class="row">
					<!-- search -->
					<div class="col-10 agileits_search">
						<form class="form-inline" action="#" method="post">
							<input class="form-control mr-sm-2" type="search"
								placeholder="Search" aria-label="Search" required>
							<button class="btn my-2 my-sm-0" type="submit">Search</button>
						</form>
					</div>
					<!-- //search -->
					<!-- cart details -->
					<div class="col-2 top_nav_right text-center mt-sm-0 mt-2">
						<div class="wthreecartaits wthreecartaits2 cart cart box_1">

							<a href="<c:url value="/gio-hang" />"><button
									class="btn w3view-cart" type="submit" name="submit" value="">
									<i class="fas fa-cart-arrow-down"></i>
								</button></a>
						</div>
					</div>
					<!-- //cart details -->
				</div>
			</div>
		</div>
	</div>
</div>
<!-- shop locator (popup) -->
<!-- //header-bottom -->
<!-- navigation -->
<div class="navbar-inner">
	<div class="container">
		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarSupportedContent"
				aria-controls="navbarSupportedContent" aria-expanded="false"
				aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarSupportedContent">
				<ul class="navbar-nav ml-auto text-center mr-xl-5">
					<li class="nav-item dropdown mr-lg-2 mb-lg-0 mb-2"><a
						class="nav-link dropdown-toggle" href="#" role="button"
						data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
							Hãng xe </a>
						<div class="dropdown-menu">
							<c:forEach var="item" items="${ hang_xe }">
								<a class="dropdown-item"
									href="<c:url value="/xe-hoi/${ item.id_hang_xe }" />">${ item.ten_hang_xe }</a>
							</c:forEach>
						</div></li>
					<c:forEach var="item" items="${ menus }">
						<li class="nav-item"><a class="nav-link"
							href="<c:url value="/${ item.url }/" />">${ item.name }</a></li>
					</c:forEach>

				</ul>
			</div>
		</nav>
	</div>
</div>