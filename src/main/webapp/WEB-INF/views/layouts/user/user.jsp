<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@include file="/WEB-INF/views/layouts/user/taglib.jsp"%>
<!-- modals -->
<!-- log in -->
<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title text-center">Đăng Nhập</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form:form action="dang-nhap" method="POST" modelAttribute="user">
					<div class="form-group">
						<label class="col-form-label">Email</label>
						<form:input type="email" path="user" class="form-control"
							placeholder=" " />
					</div>
					<div class="form-group">
						<label class="col-form-label">Mật khẩu</label>
						<form:input type="password" path="password" class="form-control"
							placeholder=" " />
					</div>
					<div class="right-w3l">
						<input type="submit" class="form-control" value="Đăng nhập">
					</div>
					<div class="sub-w3l">
						<div class="custom-control custom-checkbox mr-sm-2">
							<input type="checkbox" class="custom-control-input"
								id="customControlAutosizing"> <label
								class="custom-control-label" for="customControlAutosizing">Remember
								me?</label>
						</div>
					</div>
					<p class="text-center dont-do mt-3">
						Don't have an account? <a href="#" data-toggle="modal"
							data-target="#exampleModal2"> Đăng ký ngay</a>
					</p>
				</form:form>
			</div>
		</div>
	</div>
</div>
<!-- register -->
<div class="modal fade" id="exampleModal2" tabindex="-1" role="dialog"
	aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">Đăng Ký</h5>
				<button type="button" class="close" data-dismiss="modal"
					aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				<form:form action="dang-ky" method="POST" modelAttribute="user">
					<div class="form-group">
						<label class="col-form-label">Họ tên</label>
						<form:input type="text" path="display_name" class="form-control"
							placeholder=" " />
					</div>
					<div class="form-group">
						<label class="col-form-label">Email</label>
						<form:input type="email" path="user" class="form-control"
							placeholder=" " />
					</div>
					<div class="form-group">
						<label class="col-form-label">Mật khẩu</label>
						<form:input type="password" path="password" class="form-control"
							placeholder=" " id="password1" />
					</div>
					<div class="form-group">
						<label class="col-form-label">Nhập lại mật khẩu</label> <input
							type="password" class="form-control" placeholder=" "
							name="Confirm Password" id="password2" required="">
					</div>
					<div class="form-group">
						<label class="col-form-label">Địa chỉ</label>
						<form:input type="text" path="address" class="form-control"
							placeholder=" " />
					</div>
					<div class="right-w3l">
						<input type="submit" class="form-control" value="Register">
					</div>
					<div class="sub-w3l">
						<div class="custom-control custom-checkbox mr-sm-2">
							<input type="checkbox" class="custom-control-input"
								id="customControlAutosizing2"> <label
								class="custom-control-label" for="customControlAutosizing2">I
								Accept to the Terms & Conditions</label>
						</div>
					</div>
				</form:form>
			</div>
		</div>
	</div>
