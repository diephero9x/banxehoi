package BanXeHoi.Dao;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import BanXeHoi.Dto.GioHangDto;
import BanXeHoi.Dto.XeHoiDto;
@Repository
public class GioHangDao extends BaseDao{
	@Autowired
	XeHoiDao xeHoiDao = new XeHoiDao();

	public HashMap<Long, GioHangDto> AddCart(long id, HashMap<Long, GioHangDto> cart) {
		GioHangDto itemCart = new GioHangDto();
		XeHoiDto product = xeHoiDao.FindDataChiTietXeHoi(id);
		if(product != null && cart.containsKey(id)) {
			itemCart = cart.get(id);
			itemCart.setSoluong(itemCart.getSoluong() + 1);
			itemCart.setTonggiaTien(itemCart.getSoluong() * itemCart.getXehoi().getDon_gia_san_pham());
		}else {
			itemCart.setXehoi(product);
			itemCart.setSoluong(1);
			itemCart.setTonggiaTien(product.getDon_gia_san_pham());
		}
		cart.put(id, itemCart);
		return cart;
	}
	public HashMap<Long, GioHangDto> EditCart(long id,int quanty, HashMap<Long, GioHangDto> cart) {
		if(cart == null) {
			return cart;
		}
		GioHangDto itemCart = new GioHangDto();
		
		if(cart.containsKey(id)) {
			itemCart = cart.get(id);
			itemCart.setSoluong(quanty);
			double totalPrice = quanty * itemCart.getXehoi().getDon_gia_san_pham();
			itemCart.setTonggiaTien(totalPrice);
		}
		cart.put(id, itemCart);
		return cart;
	}
	public HashMap<Long, GioHangDto> DeleteCart(long id, HashMap<Long, GioHangDto> cart) {
		if(cart == null) {
			return cart;
		}
		if(cart.containsKey(id)) {
			cart.remove(id);
		}
		return cart;
	}
	public int TotalQuanty(HashMap<Long, GioHangDto> cart) {
		int totalQuanty = 0;
		for(Map.Entry<Long, GioHangDto> itemCart : cart.entrySet()) {
			totalQuanty += itemCart.getValue().getSoluong();
		}
		return totalQuanty;
	}
	
	public double TotalPrice(HashMap<Long, GioHangDto> cart) {
		double totalPrice = 0;
		for(Map.Entry<Long, GioHangDto> itemCart : cart.entrySet()) {
			totalPrice += itemCart.getValue().getTonggiaTien();
		}
		return totalPrice;
	}
}
