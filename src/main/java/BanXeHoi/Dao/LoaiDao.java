package BanXeHoi.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import BanXeHoi.Entity.Loai;
import BanXeHoi.Entity.MapperLoai;

@Repository
public class LoaiDao extends BaseDao {
	public List<Loai> GetDataLoai(){
		List<Loai> list = new ArrayList<Loai>();
		String sql = "SELECT * FROM loai_san_pham";
		list = _jdbcTemplate.query(sql, new MapperLoai());
		return list;
	} 
}
