package BanXeHoi.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import BanXeHoi.Dto.XeHoiDto;
import BanXeHoi.Dto.XeHoiDtoMapper;
@Repository
public class XeHoiDao extends BaseDao {
	private final boolean YES = true;
	private final boolean NO = false;
	private StringBuffer  SqlString() {
		StringBuffer sql = new StringBuffer();
		sql.append("SELECT p.id_san_pham ");
		sql.append(", h.id_hang_xe ");
		sql.append(", h.ten_hang_xe ");
		sql.append(", p.ten_san_pham ");
		sql.append(", p.hinh_anh_sp ");
		sql.append(", p.mo_ta_san_pham ");
		sql.append(", p.don_gia_san_pham ");
		sql.append(", p.so_ghe_ngoi ");
		sql.append(", l.id_loai ");
		sql.append(", l.ten_loai ");
		sql.append(", p.giam_gia ");
		sql.append(", p.moi ");
		sql.append(", p.noi_bat ");
		sql.append(", p.created_at ");
		sql.append(", p.update_at ");
		sql.append("FROM ");
		sql.append("san_pham AS p ");
		sql.append("INNER JOIN hang_xe AS h ON p.id_hang_xe=h.id_hang_xe ");
		sql.append("INNER JOIN loai_san_pham AS l ON p.id_loai=l.id_loai ");
		return sql;
	}

	private String SqlSanPhamMoiNhat(boolean moi) {
		StringBuffer sql = SqlString();
		sql.append("WHERE 1 = 1 ");
		if (moi) {
			sql.append("AND p.moi = true ");
		}
		sql.append("GROUP BY p.id_san_pham, h.id_hang_xe, l.id_loai ");
		sql.append("ORDER BY RAND() ");
		if (moi) {
			sql.append("LIMIT 3");
		}
		return sql.toString();
	}
	private String SqlSanPhamNoiBat(boolean noibat) {
		StringBuffer sql = SqlString();
		sql.append("WHERE 1 = 1 ");
		if (noibat) {
			sql.append("AND p.noi_bat = true ");
		}
		sql.append("GROUP BY p.id_san_pham, h.id_hang_xe, l.id_loai ");
		sql.append("ORDER BY RAND() ");
		if (noibat) {
			sql.append("LIMIT 3");
		}
		return sql.toString();
	}
	private StringBuffer SqlSanPham() {
		StringBuffer sql = SqlString();
		sql.append("WHERE 1 = 1 ");
		sql.append("GROUP BY p.id_san_pham, h.id_hang_xe, l.id_loai ");
		return sql;
	}
	private String SqlChiTietSanPham(long id) {
		StringBuffer sql = SqlString();
		sql.append("WHERE 1 = 1 ");
		sql.append("AND p.id_san_pham = " + id +" ");
		return sql.toString();
	}
	private StringBuffer SqlSanPhamById(int id) {
		StringBuffer sql = SqlString();
		sql.append("WHERE 1 = 1 ");
		sql.append("AND p.id_hang_xe = " + id + " ");
		return sql;
	}
	private String SqlSanPhamPhanTrang(int id,int trangDau, int tongTrang) {
		StringBuffer sql = SqlSanPhamById(id);
		sql.append("LIMIT " + trangDau +", "+ tongTrang);
		return sql.toString();
	}
	private String SqlSanPhamPhanTrang1(int trangDau, int tongTrang) {
		StringBuffer sql = SqlSanPham();
		sql.append("LIMIT " + trangDau +", "+ tongTrang);
		return sql.toString();
	}
	public List<XeHoiDto> GetDataSanPhamMoiNhat() {
		String sql = SqlSanPhamMoiNhat(YES);
		List<XeHoiDto> listSp = _jdbcTemplate.query(sql, new XeHoiDtoMapper());
		return listSp;
	}
	public List<XeHoiDto> GetDataSanPhamNoiBat() {
		String sql = SqlSanPhamNoiBat(YES);
		List<XeHoiDto> listSp = _jdbcTemplate.query(sql, new XeHoiDtoMapper());
		return listSp;
	}
	public List<XeHoiDto> GetDataXeHoi() {
 		String sql = SqlSanPham().toString();
		List<XeHoiDto> listSp = _jdbcTemplate.query(sql, new XeHoiDtoMapper());
		return listSp;
	}
	public List<XeHoiDto> GetDataXeHoiById( int id) {
		String sql = SqlSanPhamById(id).toString();
		List<XeHoiDto> listSp = _jdbcTemplate.query(sql, new XeHoiDtoMapper());
		return listSp;
	}
	
	public List<XeHoiDto> GetDataXeHoiPhanTrang( int id,int trangDau, int tongTrang) {
		StringBuffer sqlBuffer = SqlSanPhamById(id);
		List<XeHoiDto> listSpbyID = _jdbcTemplate.query(sqlBuffer.toString(), new XeHoiDtoMapper());
		List<XeHoiDto> listSp = new ArrayList<XeHoiDto>();
		if(listSpbyID.size()>0) {
			String sql = SqlSanPhamPhanTrang(id,trangDau, tongTrang);
			listSp = _jdbcTemplate.query(sql, new XeHoiDtoMapper());
		}
		return listSp;
	}
	public List<XeHoiDto> GetDataXeHoiPhanTrang1( int trangDau, int tongTrang) {
		StringBuffer sqlBuffer = SqlSanPham();
		List<XeHoiDto> listSpbyID = _jdbcTemplate.query(sqlBuffer.toString(), new XeHoiDtoMapper());
		List<XeHoiDto> listSp = new ArrayList<XeHoiDto>();
		if(listSpbyID.size()>0) {
			String sql = SqlSanPhamPhanTrang1(trangDau, tongTrang);
			listSp = _jdbcTemplate.query(sql, new XeHoiDtoMapper());
		}
		return listSp;
	}

	public List<XeHoiDto> GetDataChiTietXeHoi(int id) {
		String sql = SqlChiTietSanPham(id);
		List<XeHoiDto> listXeHoiDto = _jdbcTemplate.query(sql, new XeHoiDtoMapper());
		return listXeHoiDto;
	}
	
	public XeHoiDto FindDataChiTietXeHoi(long id) {
		String sql = SqlChiTietSanPham(id);
		XeHoiDto xehoi = _jdbcTemplate.queryForObject(sql, new XeHoiDtoMapper());
		return xehoi;
	}
	
	
}
