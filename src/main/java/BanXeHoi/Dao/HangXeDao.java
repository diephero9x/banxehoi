package BanXeHoi.Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Repository;

import BanXeHoi.Entity.HangXe;
import BanXeHoi.Entity.MapperHangXe;
@Repository
public class HangXeDao extends BaseDao {
	public List<HangXe> GetDataHangXe(){
		List<HangXe> list = new ArrayList<HangXe>();
		String sql = "SELECT * FROM hang_xe";
		list = _jdbcTemplate.query(sql, new MapperHangXe());
		return list;
	} 
}
