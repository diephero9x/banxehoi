package BanXeHoi.Controller.User;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import BanXeHoi.Entity.Users;

@Controller
public class HomeController extends BaseController {
	@RequestMapping(value = { "/", "/trang-chu" },  method = RequestMethod.GET)
	public ModelAndView Index() {
		_mvShare.setViewName("user/index");
		return _mvShare;
	}
	
	@RequestMapping(value = { "/quan-tri/index" },  method = RequestMethod.GET)
	public ModelAndView IndexAdmin() {
		_mvShare.setViewName("admin/index");
		return _mvShare;
	}
	
	
	
}
