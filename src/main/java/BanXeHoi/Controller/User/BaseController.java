package BanXeHoi.Controller.User;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.servlet.ModelAndView;

import BanXeHoi.Entity.Users;
import BanXeHoi.Service.User.HomeServiceImpl;
@Controller
public class BaseController {
	@Autowired
	HomeServiceImpl _homeService;
	public ModelAndView _mvShare = new ModelAndView();
	
	@PostConstruct
	public ModelAndView Init () {
		_mvShare.addObject("slides", _homeService.GetDataSlide());
		_mvShare.addObject("loai_san_pham", _homeService.GetDataLoai());
		_mvShare.addObject("san_pham_moi", _homeService.GetDataSanPhamMoiNhat());
		_mvShare.addObject("san_pham_noibat", _homeService.GetDataSanPhamNoiBat());
		_mvShare.addObject("menus", _homeService.GetDataMenus());
		_mvShare.addObject("hang_xe", _homeService.GetDataHangXe());
		_mvShare.addObject("xe_hoi", _homeService.GetDataXeHoi());
		_mvShare.addObject("user", new Users());
		return _mvShare;
	}
}
