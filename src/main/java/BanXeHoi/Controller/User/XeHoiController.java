package BanXeHoi.Controller.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import BanXeHoi.Dto.PhanTrangDto;
import BanXeHoi.Service.User.PhanTrangServiceImpl;
import BanXeHoi.Service.User.XeHoiServiceImpl;
@Controller
public class XeHoiController extends BaseController{
	@Autowired
	private XeHoiServiceImpl xeImpl;
	@Autowired
	private PhanTrangServiceImpl phantrangImpl;
	
	private int limit = 9;
	
	@RequestMapping(value = {"/xe-hoi"})
	public ModelAndView AllXeHoi()
	{
		
		_mvShare.addObject("xe_hoi", _homeService.GetDataXeHoi());
		_mvShare.setViewName("user/xehoi/product");
		return _mvShare;
	}
	@RequestMapping(value = {"/xe-hoi/{id_hang_xe}"})
	public ModelAndView XeHoiTheoHang(@PathVariable String id_hang_xe)
	{
		_mvShare.setViewName("user/xehoi/xehoiphantrang");
		
		int tongData = xeImpl.GetDataXeHoiById(Integer.parseInt(id_hang_xe)).size();
		PhanTrangDto pTrangDto = phantrangImpl.GetThongTinPhanTrang(tongData, limit, 1);
		_mvShare.addObject("xe_hoi_pt", pTrangDto);
		_mvShare.addObject("xe_hoi_phan_trang", xeImpl.GetDataXeHoiPhanTrang(Integer.parseInt(id_hang_xe),pTrangDto.getTrangDau(), pTrangDto.getTrangCuoi()));
		_mvShare.addObject("idHangXe", id_hang_xe);
		return _mvShare;
	}
	@RequestMapping(value = {"/xe-hoi/{id_hang_xe}/{trangHienTai}"})
	public ModelAndView XeHoiTheoHang(@PathVariable String id_hang_xe, @PathVariable String trangHienTai)
	{
		_mvShare.setViewName("user/xehoi/xehoiphantrang");
		int tongData = xeImpl.GetDataXeHoiById(Integer.parseInt(id_hang_xe)).size();
		PhanTrangDto pTrangDto = phantrangImpl.GetThongTinPhanTrang(tongData, limit, Integer.parseInt(trangHienTai));
		_mvShare.addObject("xe_hoi_pt", pTrangDto);
		_mvShare.addObject("xe_hoi_phan_trang", xeImpl.GetDataXeHoiPhanTrang(Integer.parseInt(id_hang_xe),pTrangDto.getTrangDau(), pTrangDto.getTrangCuoi()));
		_mvShare.addObject("idHangXe", id_hang_xe);
		return _mvShare;
	}
	
	@RequestMapping(value = {"/chi-tiet-xe-hoi/{id_xe_hoi}"})
	public ModelAndView ChiTietXeHoi(@PathVariable int id_xe_hoi)
	{
		_mvShare.setViewName("user/xehoi/chitietxehoi");
		_mvShare.addObject("chi_tiet_xe_hoi", xeImpl.GetDataChiTietXeHoi(id_xe_hoi));
		return _mvShare;
	}
	//ADMIN
	@RequestMapping(value = { "/quan-tri/xe-hoi" },  method = RequestMethod.GET)
	public ModelAndView IndexAdmin() {
		_mvShare.addObject("xe_hoi", _homeService.GetDataXeHoi());
		int tongData = xeImpl.GetDataXeHoi().size();
		PhanTrangDto pTrangDto = phantrangImpl.GetThongTinPhanTrang(tongData, limit, 1);
		_mvShare.addObject("xe_hoi_pt1", pTrangDto);
		_mvShare.addObject("xe_hoi_phan_trang1", xeImpl.GetDataXeHoiPhanTrang1(pTrangDto.getTrangDau(), pTrangDto.getTrangCuoi()));
		_mvShare.setViewName("admin/allxehoi");
		return _mvShare;
	}
	@RequestMapping(value = {"/quan-tri/xe-hoi/{trangHienTai}"})
	public ModelAndView IndexAdmin1(@PathVariable String trangHienTai)
	{
		_mvShare.setViewName("admin/allxehoi");
		int tongData = xeImpl.GetDataXeHoi().size();
		PhanTrangDto pTrangDto = phantrangImpl.GetThongTinPhanTrang(tongData, limit, Integer.parseInt(trangHienTai));
		_mvShare.addObject("xe_hoi_pt1", pTrangDto);
		_mvShare.addObject("xe_hoi_phan_trang1", xeImpl.GetDataXeHoiPhanTrang1(pTrangDto.getTrangDau(), pTrangDto.getTrangCuoi()));

		return _mvShare;
	}
}
