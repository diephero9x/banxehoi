package BanXeHoi.Controller.User;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

import BanXeHoi.Entity.Users;
import BanXeHoi.Service.User.AccountServiceImpl;

@Controller
public class UserController extends BaseController{
	@Autowired
	AccountServiceImpl accountService = new AccountServiceImpl();
	
	@RequestMapping(value = { "/dang-ky" }, method = RequestMethod.POST)
	public ModelAndView DangKy(@ModelAttribute("user") Users user) {
		int count = accountService.AddAccount(user);
		if (count>0) {
			_mvShare.addObject("status", "Đăng ký tài khoản thành công!");
		}
		else {
			_mvShare.addObject("status", "Đăng ký tài khoản thất bại!");
		}

		_mvShare.setViewName("user/index");
			return _mvShare;
	}
	
	@RequestMapping(value = { "/dang-nhap" }, method = RequestMethod.POST)
	public ModelAndView DangNhap(HttpSession session,@ModelAttribute("user") Users user) {
		 user = accountService.KiemTraTaiKhoan(user);
		 if (user != null) {
				_mvShare.setViewName("redirect:trang-chu");
			 	session.setAttribute("LoginInfo", user);	 	
			}
			else {
				_mvShare.addObject("statusLogin", "Đăng nhập thất bại!");
				_mvShare.setViewName("redirect:trang-chu");
			}	
			return _mvShare;
	}
	
	
	@RequestMapping(value = { "/dang-xuat" }, method = RequestMethod.GET)
	public String DangXuat(HttpSession session,HttpServletRequest request) {
		session.removeAttribute("LoginInfo");
		
		return "redirect:"+request.getHeader("Referer");
	}
}
