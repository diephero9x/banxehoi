package BanXeHoi.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import BanXeHoi.Dao.XeHoiDao;
import BanXeHoi.Dto.XeHoiDto;
@Service
public class XeHoiServiceImpl implements IXeHoiService{
	@Autowired
	 private XeHoiDao sanPhamDao;
	public List<XeHoiDto> GetDataXeHoiPhanTrang(int id,int trangDau, int tongTrang) {
		return sanPhamDao.GetDataXeHoiPhanTrang(id,trangDau, tongTrang);
	}
	public List<XeHoiDto> GetDataXeHoiPhanTrang1(int trangDau, int tongTrang) {
		return sanPhamDao.GetDataXeHoiPhanTrang1(trangDau, tongTrang);
	}
	public List<XeHoiDto> GetDataXeHoiById(int id) {
		// TODO Auto-generated method stub
		return sanPhamDao.GetDataXeHoiById(id);
	}
	public List<XeHoiDto> GetDataXeHoi() {
		// TODO Auto-generated method stub
		return sanPhamDao.GetDataXeHoi();
	}
	public XeHoiDto GetDataChiTietXeHoi(int id) {
		// TODO Auto-generated method stub
		List<XeHoiDto> list = sanPhamDao.GetDataChiTietXeHoi(id);
		return list.get(0);
	}

}
