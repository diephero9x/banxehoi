package BanXeHoi.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import BanXeHoi.Dto.XeHoiDto;
import BanXeHoi.Entity.HangXe;
import BanXeHoi.Entity.Loai;
import BanXeHoi.Entity.Menus;
import BanXeHoi.Entity.Slides;
@Service
public interface IHomeService {
	@Autowired
	public List<Slides> GetDataSlide();
	public List<Loai> GetDataLoai();
	public List<Menus> GetDataMenus();
	public List<XeHoiDto> GetDataSanPhamMoiNhat();
	public List<XeHoiDto> GetDataSanPhamNoiBat();
	public List<XeHoiDto> GetDataXeHoi();
	public List<HangXe> GetDataHangXe();
}
