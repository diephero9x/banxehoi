package BanXeHoi.Service.User;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import BanXeHoi.Dao.GioHangDao;
import BanXeHoi.Dto.GioHangDto;

@Service
public class GioHangServiceImpl {
	@Autowired
	private GioHangDao cartDao = new GioHangDao();

	public HashMap<Long, GioHangDto> AddCart(long id, HashMap<Long, GioHangDto> cart) {
		return cartDao.AddCart(id, cart);
	}

	public HashMap<Long, GioHangDto> EditCart(long id, int quanty, HashMap<Long, GioHangDto> cart) {
		return cartDao.EditCart(id, quanty, cart);
	}

	public HashMap<Long, GioHangDto> DeleteCart(long id, HashMap<Long, GioHangDto> cart) {
		return cartDao.DeleteCart(id, cart);
	}

	public int TotalQuanty(HashMap<Long, GioHangDto> cart) {
		return cartDao.TotalQuanty(cart);
	}

	public double TotalPrice(HashMap<Long, GioHangDto> cart) {
		return cartDao.TotalPrice(cart);
	}
}
