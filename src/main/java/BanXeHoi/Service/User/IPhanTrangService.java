package BanXeHoi.Service.User;

import org.springframework.stereotype.Service;

import BanXeHoi.Dto.PhanTrangDto;

@Service
public interface IPhanTrangService {
	public PhanTrangDto GetThongTinPhanTrang(int tongTrang, int gioiHan, int trangHienTai);
}
