package BanXeHoi.Service.User;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import BanXeHoi.Dao.HangXeDao;
import BanXeHoi.Dao.LoaiDao;
import BanXeHoi.Dao.MenuDao;
import BanXeHoi.Dao.XeHoiDao;
import BanXeHoi.Dao.SlidesDao;
import BanXeHoi.Dto.XeHoiDto;
import BanXeHoi.Entity.HangXe;
import BanXeHoi.Entity.Loai;
import BanXeHoi.Entity.Menus;
import BanXeHoi.Entity.Slides;
@Service
public class HomeServiceImpl implements IHomeService{
	@Autowired
	private SlidesDao slidesDao;
	@Autowired
	private LoaiDao loaiDao;
	@Autowired
	private MenuDao menuDao;
	@Autowired
	private XeHoiDao spDao;
	@Autowired
	private HangXeDao hangXeDao;
	
	public List<Slides> GetDataSlide(){
		return slidesDao.GetDataSlide();
	}
	public List<Loai> GetDataLoai() {
		// TODO Auto-generated method stub
		return loaiDao.GetDataLoai();
	}
	public List<Menus> GetDataMenus() {
		return menuDao.GetDataMenus();
	}
	public List<XeHoiDto> GetDataSanPhamMoiNhat() {
		// TODO Auto-generated method stub
		List<XeHoiDto> listSp = spDao.GetDataSanPhamMoiNhat();
		return listSp;
	}
	public List<XeHoiDto> GetDataSanPhamNoiBat() {
		// TODO Auto-generated method stub
		List<XeHoiDto> listSp = spDao.GetDataSanPhamNoiBat();
		return listSp;
	}
	public List<HangXe> GetDataHangXe() {
		return hangXeDao.GetDataHangXe();
	}
	
	//Dánh sach xe
	public List<XeHoiDto> GetDataXeHoi() {
		List<XeHoiDto> listSp = spDao.GetDataXeHoi();
		return listSp;
	}
}
