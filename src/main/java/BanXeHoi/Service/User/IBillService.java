package BanXeHoi.Service.User;

import java.util.HashMap;

import org.springframework.stereotype.Service;

import BanXeHoi.Dto.GioHangDto;
import BanXeHoi.Entity.Bills;

@Service
public interface IBillService {
	public int AddBills(Bills bills);
	public void AddBillsDetails(HashMap<Long, GioHangDto> carts);
}
