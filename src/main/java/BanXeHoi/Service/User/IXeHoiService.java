package BanXeHoi.Service.User;

import java.util.List;

import org.springframework.stereotype.Service;

import BanXeHoi.Dto.XeHoiDto;
@Service
public interface IXeHoiService {
	public List<XeHoiDto> GetDataXeHoiById( int id);
	public List<XeHoiDto> GetDataXeHoiPhanTrang(int id, int trangDau, int tongTrang);
	public XeHoiDto GetDataChiTietXeHoi(int id);
	public List<XeHoiDto> GetDataXeHoi();
	public List<XeHoiDto> GetDataXeHoiPhanTrang1(int trangDau, int tongTrang);
}
