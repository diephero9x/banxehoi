package BanXeHoi.Service.User;

import org.springframework.stereotype.Service;
import java.util.HashMap;
import BanXeHoi.Dto.GioHangDto;;
@Service
public interface IGioHangService {
	
	public HashMap<Long, GioHangDto> AddCart(long id, HashMap<Long, GioHangDto> cart);
	public HashMap<Long, GioHangDto> EditCart(long id, int quanty, HashMap<Long, GioHangDto> cart);
	public HashMap<Long, GioHangDto> DeleteCart(long id, HashMap<Long, GioHangDto> cart);
	public int TotalQuanty(HashMap<Long, GioHangDto> cart);
	public double TotalPrice(HashMap<Long, GioHangDto> cart);
}
