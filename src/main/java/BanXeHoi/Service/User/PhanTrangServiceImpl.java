package BanXeHoi.Service.User;

import org.springframework.stereotype.Service;

import BanXeHoi.Dto.PhanTrangDto;

@Service
public class PhanTrangServiceImpl implements IPhanTrangService {
	public PhanTrangDto GetThongTinPhanTrang(int tongData, int gioiHan, int trangHienTai) {
		PhanTrangDto phanTrangDto = new PhanTrangDto();
		phanTrangDto.setGioiHan(gioiHan);
		phanTrangDto.setTongTrang(SetTongData(tongData, gioiHan));
		phanTrangDto.setTrangHienTai(CheckCurrentPage(trangHienTai, phanTrangDto.getTongTrang()));
		int start = FindStart(phanTrangDto.getTrangHienTai(), gioiHan);
		phanTrangDto.setTrangDau(start);
		int end = FindEnd(phanTrangDto.getTrangDau(), gioiHan, tongData);
		phanTrangDto.setTrangCuoi(end);
		return phanTrangDto;
	}

	private int FindEnd(int trangDau, int gioiHan, int tongData) {
		// TODO Auto-generated method stub
		return trangDau + gioiHan > tongData ? tongData : (trangDau + gioiHan) -1;
	}

	private int SetTongData(int tongData, int gioiHan) {
		int totalPage = 0;
		totalPage = tongData / gioiHan;
		totalPage = totalPage * gioiHan < tongData ? totalPage + 1 : totalPage;
		return totalPage;
		
	}
	
	public int CheckCurrentPage(int trangHienTai, int tongTrang) {
		if (trangHienTai < 1) {
			return 1;
		}
		if (trangHienTai > tongTrang) {
			return tongTrang;
		}
		return trangHienTai;
	}
	private int FindStart(int trangHienTai, int gioiHan) {
		return ((trangHienTai - 1) * gioiHan) + 1;
	}
	
}
