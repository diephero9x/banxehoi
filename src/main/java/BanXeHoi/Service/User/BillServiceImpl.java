package BanXeHoi.Service.User;

import java.util.HashMap;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import BanXeHoi.Dao.BillsDao;
import BanXeHoi.Dto.GioHangDto;
import BanXeHoi.Entity.BillDetail;
import BanXeHoi.Entity.Bills;

@Service
public class BillServiceImpl implements IBillService {
	@Autowired
	private BillsDao billsDao;

	public int AddBills(Bills bills) {
		return billsDao.AddBills(bills);
	}

	public void AddBillsDetails(HashMap<Long, GioHangDto> carts) {
		long idBills = billsDao.GetIDLastBills();
		for(Map.Entry<Long, GioHangDto> itemCart : carts.entrySet()) {
			BillDetail billDetail = new BillDetail();
			billDetail.setId_bills(idBills);
			billDetail.setId_product(itemCart.getValue().getXehoi().getId_san_pham());;
			billDetail.setQuanty(itemCart.getValue().getSoluong());
			billDetail.setTotal(itemCart.getValue().getTonggiaTien());
			billsDao.AddBillsDetail(billDetail);
		}
		// TODO Auto-generated method stub
		
	}
	
	
}
