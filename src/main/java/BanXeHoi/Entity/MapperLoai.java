package BanXeHoi.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperLoai implements RowMapper<Loai> {

	public Loai mapRow(ResultSet rs, int rowNum) throws SQLException {
		Loai loai= new Loai();
		loai.setId_loai(rs.getInt("id_loai"));
		loai.setTen_loai(rs.getString("ten_loai"));
		loai.setMo_ta_loai(rs.getString("mo_ta_loai"));
		return loai;
	}
	
}
