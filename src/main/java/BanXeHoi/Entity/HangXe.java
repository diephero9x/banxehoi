package BanXeHoi.Entity;

public class HangXe {
	private int id_hang_xe;
	private String ten_hang_xe;
	private String logo_hang_xe;

	public HangXe() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getId_hang_xe() {
		return id_hang_xe;
	}

	public void setId_hang_xe(int id_hang_xe) {
		this.id_hang_xe = id_hang_xe;
	}

	public String getTen_hang_xe() {
		return ten_hang_xe;
	}

	public void setTen_hang_xe(String ten_hang_xe) {
		this.ten_hang_xe = ten_hang_xe;
	}

	public String getLogo_hang_xe() {
		return logo_hang_xe;
	}

	public void setLogo_hang_xe(String logo_hang_xe) {
		this.logo_hang_xe = logo_hang_xe;
	}

}
