package BanXeHoi.Entity;

public class Loai {
	private int id_loai;
	private String ten_loai;
	public int getId_loai() {
		return id_loai;
	}
	public Loai() {
		super();
	}
	public void setId_loai(int id_loai) {
		this.id_loai = id_loai;
	}
	public String getTen_loai() {
		return ten_loai;
	}
	public void setTen_loai(String ten_loai) {
		this.ten_loai = ten_loai;
	}
	public String getMo_ta_loai() {
		return mo_ta_loai;
	}
	public void setMo_ta_loai(String mo_ta_loai) {
		this.mo_ta_loai = mo_ta_loai;
	}
	private String mo_ta_loai;
}
