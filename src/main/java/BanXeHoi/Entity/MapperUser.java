package BanXeHoi.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperUser implements RowMapper<Users> {

	public Users mapRow(ResultSet rs, int rowNum) throws SQLException {
		Users user = new Users();
		user.setId_user(rs.getLong("id_user"));
		user.setUser(rs.getString("user"));
		user.setPassword(rs.getString("password"));
		user.setAddress(rs.getString("address"));
		user.setDisplay_name(rs.getString("display_name"));
		return user;
	}
	
}
