package BanXeHoi.Entity;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class MapperHangXe implements RowMapper<HangXe>{

	public HangXe mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		HangXe hangXe= new HangXe();
		hangXe.setId_hang_xe(rs.getInt("id_hang_xe"));
		hangXe.setTen_hang_xe(rs.getString("ten_hang_xe"));
		hangXe.setLogo_hang_xe(rs.getString("logo_hang_xe"));
		return hangXe;
	}

}
