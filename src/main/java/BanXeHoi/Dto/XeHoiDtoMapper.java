package BanXeHoi.Dto;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.jdbc.core.RowMapper;

public class XeHoiDtoMapper implements RowMapper<XeHoiDto> {

	public XeHoiDto mapRow(ResultSet rs, int rowNum) throws SQLException {
		// TODO Auto-generated method stub
		XeHoiDto sPhamDto = new XeHoiDto();
		sPhamDto.setId_san_pham(rs.getInt("id_san_pham"));
		sPhamDto.setId_hang_xe(rs.getInt("id_hang_xe"));
		sPhamDto.setTen_hang_xe(rs.getString("ten_hang_xe"));
		sPhamDto.setTen_san_pham(rs.getString("ten_san_pham"));
		sPhamDto.setHinh_anh_sp(rs.getString("hinh_anh_sp"));
		sPhamDto.setMo_ta_san_pham(rs.getString("mo_ta_san_pham"));
		sPhamDto.setDon_gia_san_pham(rs.getDouble("don_gia_san_pham"));
		sPhamDto.setSo_ghe_ngoi(rs.getInt("so_ghe_ngoi"));
		sPhamDto.setId_loai(rs.getInt("id_loai"));
		sPhamDto.setTen_loai(rs.getString("ten_loai"));
		sPhamDto.setGiam_gia(rs.getInt("giam_gia"));
		sPhamDto.setMoi(rs.getBoolean("moi"));
		sPhamDto.setNoi_bat(rs.getBoolean("noi_bat"));
		sPhamDto.setCreated_at(rs.getDate("created_at"));
		sPhamDto.setUpdate_at(rs.getDate("update_at"));

		return sPhamDto;
	}

}
