package BanXeHoi.Dto;

import java.sql.Date;

public class XeHoiDto {
	private int id_san_pham;
	private int id_hang_xe;
	private String ten_hang_xe;
	private String ten_san_pham;
	private String hinh_anh_sp;
	private String mo_ta_san_pham;
	private double don_gia_san_pham;
	private int so_ghe_ngoi;
	private int id_loai;
	private String ten_loai;
	private int giam_gia;
	private boolean moi;
	private boolean noi_bat;
	private Date created_at;
	private Date update_at;
	public XeHoiDto() {
		super();
		// TODO Auto-generated constructor stub
	}
	public int getId_san_pham() {
		return id_san_pham;
	}
	public void setId_san_pham(int id_san_pham) {
		this.id_san_pham = id_san_pham;
	}
	public int getId_hang_xe() {
		return id_hang_xe;
	}
	public void setId_hang_xe(int id_hang_xe) {
		this.id_hang_xe = id_hang_xe;
	}
	public String getTen_hang_xe() {
		return ten_hang_xe;
	}
	public void setTen_hang_xe(String ten_hang_xe) {
		this.ten_hang_xe = ten_hang_xe;
	}
	public String getTen_san_pham() {
		return ten_san_pham;
	}
	public void setTen_san_pham(String ten_san_pham) {
		this.ten_san_pham = ten_san_pham;
	}
	public String getHinh_anh_sp() {
		return hinh_anh_sp;
	}
	public void setHinh_anh_sp(String hinh_anh_sp) {
		this.hinh_anh_sp = hinh_anh_sp;
	}
	public String getMo_ta_san_pham() {
		return mo_ta_san_pham;
	}
	public void setMo_ta_san_pham(String mo_ta_san_pham) {
		this.mo_ta_san_pham = mo_ta_san_pham;
	}
	public double getDon_gia_san_pham() {
		return don_gia_san_pham;
	}
	public void setDon_gia_san_pham(double don_gia_san_pham) {
		this.don_gia_san_pham = don_gia_san_pham;
	}
	public int getSo_ghe_ngoi() {
		return so_ghe_ngoi;
	}
	public void setSo_ghe_ngoi(int so_ghe_ngoi) {
		this.so_ghe_ngoi = so_ghe_ngoi;
	}
	public int getId_loai() {
		return id_loai;
	}
	public void setId_loai(int id_loai) {
		this.id_loai = id_loai;
	}
	public String getTen_loai() {
		return ten_loai;
	}
	public void setTen_loai(String ten_loai) {
		this.ten_loai = ten_loai;
	}
	public int getGiam_gia() {
		return giam_gia;
	}
	public void setGiam_gia(int giam_gia) {
		this.giam_gia = giam_gia;
	}
	public boolean isMoi() {
		return moi;
	}
	public void setMoi(boolean moi) {
		this.moi = moi;
	}
	public boolean isNoi_bat() {
		return noi_bat;
	}
	public void setNoi_bat(boolean noi_bat) {
		this.noi_bat = noi_bat;
	}
	public Date getCreated_at() {
		return created_at;
	}
	public void setCreated_at(Date created_at) {
		this.created_at = created_at;
	}
	public Date getUpdate_at() {
		return update_at;
	}
	public void setUpdate_at(Date update_at) {
		this.update_at = update_at;
	}
	
	
}
