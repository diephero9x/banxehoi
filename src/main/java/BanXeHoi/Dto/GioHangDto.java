package BanXeHoi.Dto;

public class GioHangDto {
	private int soluong;
	private double tonggiaTien;
	private XeHoiDto xehoi;
	public GioHangDto() {
		
	}
	public GioHangDto(int soluong, double tonggiaTien, XeHoiDto xehoi) {
		super();
		this.soluong = soluong;
		this.tonggiaTien = tonggiaTien;
		this.xehoi = xehoi;
	}
	public int getSoluong() {
		return soluong;
	}
	public void setSoluong(int soluong) {
		this.soluong = soluong;
	}
	public double getTonggiaTien() {
		return tonggiaTien;
	}
	public void setTonggiaTien(double tonggiaTien) {
		this.tonggiaTien = tonggiaTien;
	}
	public XeHoiDto getXehoi() {
		return xehoi;
	}
	public void setXehoi(XeHoiDto xehoi) {
		this.xehoi = xehoi;
	}
	
	
}
