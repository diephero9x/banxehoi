package BanXeHoi.Dto;

public class PhanTrangDto {
	private int trangHienTai;
	private int gioiHan;
	private int trangDau;
	private int trangCuoi;
	private int tongTrang;

	public PhanTrangDto() {
		super();
		// TODO Auto-generated constructor stub
	}

	public int getTrangHienTai() {
		return trangHienTai;
	}

	public void setTrangHienTai(int trangHienTai) {
		this.trangHienTai = trangHienTai;
	}

	public int getGioiHan() {
		return gioiHan;
	}

	public void setGioiHan(int gioiHan) {
		this.gioiHan = gioiHan;
	}

	public int getTrangDau() {
		return trangDau;
	}

	public void setTrangDau(int trangDau) {
		this.trangDau = trangDau;
	}

	public int getTrangCuoi() {
		return trangCuoi;
	}

	public void setTrangCuoi(int trangCuoi) {
		this.trangCuoi = trangCuoi;
	}

	public int getTongTrang() {
		return tongTrang;
	}

	public void setTongTrang(int tongTrang) {
		this.tongTrang = tongTrang;
	}
}
